// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "AICharacter.h"
#include "PlayerStatsHUD.h"
#include "Animation/AnimMontage.h"
#include "RPG_GameCharacter.generated.h"


UCLASS(config=Game)
class ARPG_GameCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UMotionWarpingComponent* MotionWarping;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UAttackSystem* attackSystem;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* LookAction;

	/** Crouch Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* CrouchAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* VaultAction;


	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* AttackAction;

public:
	ARPG_GameCharacter();
	

protected:

	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);


	/** Called for looking input */
	void OnCrouchEvent(const FInputActionValue& Value);
	void OnStopCrouchEvent(const FInputActionValue& Value);
	void Vault(const FInputActionValue& Value);

	void VaultHit(const int hitIndex, const FHitResult& hit);
	bool VaultLanding(const int hitIndex, const FHitResult& hit);

	void Attack(const FInputActionValue& Value);

	UFUNCTION(BlueprintCallable) virtual void CamUpdate(float Value);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable) void OnCrouch();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable) void OnStopCrouch();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable) void VaultMotionWrap();

	UFUNCTION(BlueprintCallable) void VaultMotionWrapCode();
	void AddOrUpdateWarp(FName warpTargetName, FVector& location);
	void OnCrouch_Implementation() {}
	void OnStopCrouch_Implementation() {}
	void VaultMotionWrap_Implementation() {}
	void StealthAttackCallback(UAnimMontage* animMontage, bool bInterrupted);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Vault, meta = (AllowPrivateAccess = "true")) FVector vaultStartPos;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Vault, meta = (AllowPrivateAccess = "true")) FVector vaultMidPos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Vault, meta = (AllowPrivateAccess = "true")) FVector vaultLandPos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Vault, meta = (AllowPrivateAccess = "true")) UAnimMontage* vaultAnimMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Vault, meta = (AllowPrivateAccess = "true")) UAnimMontage* stealthAttackAnimMontage;
	void VaultMotionWrapComplete(UAnimMontage* animMontage, bool bInterrupted);

	FVector targetAICharacterLocation;
	FRotator targetAICharacterRotation;

	class UInteractionMessageUserWidget* interactionMessageUserWidget;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UPROPERTY(BlueprintReadWrite) bool isCrouched;
	UPROPERTY(BlueprintReadWrite) bool canWarp;
	UPROPERTY(BlueprintReadWrite) bool isSprinting;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Vault, meta = (AllowPrivateAccess = "true")) TEnumAsByte<ETraceTypeQuery> traceChannel;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Vault, meta = (AllowPrivateAccess = "true")) float traceRadius;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Vault, meta = (AllowPrivateAccess = "true")) float traceValtingDistance;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats") class UPlayerStats* playerStats;

	UFUNCTION(BlueprintCallable) void Damage(float dmg);
	UFUNCTION(BlueprintCallable) void Heal(float hp);
	UFUNCTION(BlueprintCallable) void GainXP(float xp);
	UFUNCTION(BlueprintCallable) void UseMana(float mana);
	UFUNCTION(BlueprintCallable) void GainMana(float mana);
	UFUNCTION(BlueprintCallable) void OnDeath();
};

