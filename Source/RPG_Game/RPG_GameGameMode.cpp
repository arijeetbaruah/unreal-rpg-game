// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG_GameGameMode.h"
#include "RPG_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARPG_GameGameMode::ARPG_GameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
