// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "AttackSystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RPG_GAME_API UAttackSystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAttackSystem();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true")) class UInputAction* AttackAction;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true")) bool canAttack = true;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true")) bool isAttacking;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true")) bool saveAttack;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true")) int attackIndex = 0;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void SetupPlayerInputComponent(class UEnhancedInputComponent* EnhancedInputComponent);
	UFUNCTION() void AttackStart(const FInputActionValue& Value);
	UFUNCTION() void AttackEnd(const FInputActionValue& Value);

	UFUNCTION(BlueprintCallable) void SwordAttack();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable) void PlaySwordAttack();
	UFUNCTION(BlueprintCallable) void SwordAttackCombo();
	UFUNCTION(BlueprintCallable) void SwordAttackComboEnd();

	void PlaySwordAttack_Implementation() {}
};
