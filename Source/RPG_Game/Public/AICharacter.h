// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "IAssasination.h"
#include "AICharacter.generated.h"

UCLASS()
class RPG_GAME_API AAICharacter : public ACharacter, public IIAssasination
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* AssasinationRadius;

public:
	// Sets default values for this character's properties
	AAICharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	class UInteractionMessageUserWidget* interactionWidget;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Vault, meta = (AllowPrivateAccess = "true")) UAnimMontage* stealthKillAnimMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Vault, meta = (AllowPrivateAccess = "true")) FVector assasinLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Vault, meta = (AllowPrivateAccess = "true")) FRotator assasinRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Vault, meta = (AllowPrivateAccess = "true")) TSubclassOf<class UInteractionMessageUserWidget> InteractionTextWidget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true")) USoundBase* onDeadAudio;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable) void StealthBackAssasin(FVector& refPosition, FRotator& assasinRotator);
	void StealthBackAssasin_Implementation(FVector& refPosition, FRotator& assasinRotator);
	UFUNCTION(BlueprintCallable) void Ragdoll();
	void RagdollDelayCallback();

	bool isDeath;
	UFUNCTION(BlueprintCallable) void OnAssasianationOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintCallable) void OnAssasianationOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
