// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerStats.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RPG_GAME_API UPlayerStats : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float maxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float currentHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float currentMana;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float maxMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float currentXP
;
	int xpRequirementedToLevelUp;
	int previousXpRequirementedToLevelUp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float currentStamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float maxStamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float staminaRechargeRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float staminaUseRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) int level;

public:	
	// Sets default values for this component's properties
	UPlayerStats();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable) bool TakeDamage(float dmg);
	UFUNCTION(BlueprintCallable) void Heal(float hp);
	UFUNCTION(BlueprintCallable) void GainXP(float xp);
	UFUNCTION(BlueprintCallable) void UseMana(float mana);
	UFUNCTION(BlueprintCallable) void GainMana(float mana);
	UFUNCTION(BlueprintCallable) void LevelUp();
	UFUNCTION(BlueprintCallable) bool UseStamina();
	UFUNCTION(BlueprintCallable) void GainStamina();
	UFUNCTION(BlueprintCallable) int GetRequirementedXP(int lvl);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = "true")) TSubclassOf<class UPlayerStatsHUD> BPplayerStatsHUD;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = "true")) TSubclassOf<class ULevelUpSliderWidget> BPplayerLevelUpHUD;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") class UPlayerStatsHUD* playerStatsHUD;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") class ULevelUpSliderWidget* playerLevelUpHUD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") class UCharacterLevelData* xpToLevelMap;
};
