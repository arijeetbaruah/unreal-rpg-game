// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/RichTextBlock.h"
#include "InteractionMessageUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_GAME_API UInteractionMessageUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BluePrintReadWrite, Category = "Buttons", meta = (BindWidget)) URichTextBlock* InteractMessage;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable) void OnCrouch();
	void OnCrouch_Implementation() {}
};
