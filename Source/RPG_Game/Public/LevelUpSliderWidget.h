// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelUpSliderWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_GAME_API ULevelUpSliderWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget)) class UProgressBar* XPBar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget)) class UTextBlock* LvlTxt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget)) class UTextBlock* XPTxt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float previousXP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float maxXP;

private:
	float currentXP;
	float targetXP;

	float cooldown;
	
public:
	UFUNCTION(BlueprintCallable) void SetXP(float newCurrentXP, float newMaxXP, float previousMaxXP);
	UFUNCTION(BlueprintCallable) void SetLevel(int newLevel);

	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
};
