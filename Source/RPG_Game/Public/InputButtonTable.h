// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "InputButtonTable.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct RPG_GAME_API FInputButtonTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere) FString key;
	UPROPERTY(EditAnywhere) UTexture* front;
};
