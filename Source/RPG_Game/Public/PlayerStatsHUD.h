// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "PlayerStatsHUD.generated.h"

/**
 * 
 */
UCLASS()
class RPG_GAME_API UPlayerStatsHUD : public UUserWidget
{
	GENERATED_BODY()

private:
	float currentHP;
	float currentMana;

	float targetHP;
	float targetMana;

public:

	UPROPERTY(meta = (BindWidget)) UProgressBar* hpBar;
	UPROPERTY(meta = (BindWidget)) UProgressBar* Mana;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) float HPInterpSpeed;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float ManaInterpSpeed;

	UFUNCTION(BlueprintCallable) void SetHP(float currentHP, float maxHP);
	UFUNCTION(BlueprintCallable) void SetMana(float currentMana, float maxMana);

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
};
