// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG_GameCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "MotionWarpingComponent.h"
#include "IAssasination.h"
#include "AICharacter.h"
#include "PlayerStats.h"
#include "AttackSystem.h"
#include "InteractionMessageUserWidget.h"
#include "EnhancedInputSubsystems.h"


//////////////////////////////////////////////////////////////////////////
// ARPG_GameCharacter

const float DefaultCamArmLength = 400.0f;

ARPG_GameCharacter::ARPG_GameCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = DefaultCamArmLength; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	MotionWarping = CreateDefaultSubobject<UMotionWarpingComponent>(TEXT("MotionWarping"));
	playerStats = CreateDefaultSubobject<UPlayerStats>(TEXT("PlayerStats"));
	attackSystem = CreateDefaultSubobject<UAttackSystem>(TEXT("AttackSystem"));

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)
}

void ARPG_GameCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void ARPG_GameCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ARPG_GameCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ARPG_GameCharacter::Look);

		//Crouch
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Started, this, &ARPG_GameCharacter::OnCrouchEvent);
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Completed, this, &ARPG_GameCharacter::OnStopCrouchEvent);

		//Vaulting
		EnhancedInputComponent->BindAction(VaultAction, ETriggerEvent::Triggered, this, &ARPG_GameCharacter::Vault);


		//Vaulting
		EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Triggered, this, &ARPG_GameCharacter::Attack);

		attackSystem->SetupPlayerInputComponent(EnhancedInputComponent);

	}

}

void ARPG_GameCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		isSprinting = isSprinting && playerStats->UseStamina();
		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y * (isSprinting ? 2 : 1));
		AddMovementInput(RightDirection, MovementVector.X * (isSprinting ? 2 : 1));
	}
}

void ARPG_GameCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ARPG_GameCharacter::OnCrouchEvent(const FInputActionValue& Value)
{
	isCrouched = true;
	OnCrouch();
	GetCharacterMovement()->MaxWalkSpeed = 350.f;
}

void ARPG_GameCharacter::OnStopCrouchEvent(const FInputActionValue& Value)
{
	isCrouched = false;
	OnStopCrouch();
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
}

void ARPG_GameCharacter::CamUpdate(float Value)
{
	UE_LOG(LogTemp, Warning, TEXT("Value: %f"), Value);
}

void ARPG_GameCharacter::Vault(const FInputActionValue& Value)
{
	TArray<AActor*> ignoreActors;
	ignoreActors.Init(this, 1);
	canWarp = false;
	isSprinting = true;
	for (int index = 0; index <= 2; index++)
	{
		FVector startPos = this->GetActorLocation() + FVector(0, 0, index * 30);
		FVector forwardVector = UKismetMathLibrary::GetForwardVector(this->GetActorRotation());

		FVector endPos = forwardVector * 180 + startPos;

		FHitResult hit;
		if (UKismetSystemLibrary::SphereTraceSingle(this->GetWorld(), startPos, endPos, traceRadius, traceChannel, false, ignoreActors, EDrawDebugTrace::Type::ForDuration, hit, true))
		{
			VaultHit(index, hit);
			break;
		}
	}
}

void ARPG_GameCharacter::VaultHit(const int hitIndex, const FHitResult& hit)
{
	TArray<AActor*> ignoreActors;
	ignoreActors.Init(this, 1);

	for (int index = 0; index < 6; index++)
	{
		FVector forwardVector = UKismetMathLibrary::GetForwardVector(this->GetActorRotation());
		
		FVector startPos = hit.ImpactPoint + FVector(0, 0, 100) + (forwardVector * index * 50);
		FVector endPos = startPos - FVector(0, 0, 100);

		FHitResult nextHit;

		if (UKismetSystemLibrary::SphereTraceSingle(this->GetWorld(), startPos, endPos, traceValtingDistance, traceChannel, false, ignoreActors, EDrawDebugTrace::Type::ForDuration, nextHit, true))
		{
			if (hitIndex == 0)
			{
				vaultStartPos = nextHit.ImpactPoint;

				UKismetSystemLibrary::DrawDebugSphere(this->GetWorld(), vaultStartPos, 100.0f, 12, FLinearColor::Blue, 10, 2);
			}
			vaultMidPos = nextHit.ImpactPoint;
			canWarp = true;
		}
		else
		{
			if (VaultLanding(index, nextHit))
			{
				break;
			}
		}
	}

	VaultMotionWrap();
}

bool ARPG_GameCharacter::VaultLanding(const int hitIndex, const FHitResult& hit)
{
	TArray<AActor*> ignoreActors;
	ignoreActors.Init(this, 1);

	for (int index = 0; index < 6; index++)
	{
		FVector forwardVector = UKismetMathLibrary::GetForwardVector(this->GetActorRotation());
		
		FVector startPos = hit.TraceStart + forwardVector * 80;
		FVector endPos = startPos - FVector(0, 0, 1000);

		FHitResult nextHit;

		if (UKismetSystemLibrary::SphereTraceSingle(this->GetWorld(), startPos, endPos, traceValtingDistance, traceChannel, false, ignoreActors, EDrawDebugTrace::Type::ForDuration, nextHit, true, FLinearColor::Red))
		{
			vaultLandPos = nextHit.ImpactPoint;
			return true;
		}
	}

	return false;
}

const float range = 100.0f;

void ARPG_GameCharacter::VaultMotionWrapCode()
{
	FVector location = this->GetActorLocation();

	if ((location.Z - range <= vaultLandPos.Z && location.Z + range >= vaultLandPos.Z) && canWarp)
	{
		this->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
		this->SetActorEnableCollision(false);
		AddOrUpdateWarp(FName("Valut Start"), vaultStartPos);
		AddOrUpdateWarp(FName("VaultMiddle"), vaultMidPos);
		AddOrUpdateWarp(FName("VaultLand"), vaultLandPos);
		UAnimInstance* animInstance = this->GetMesh()->GetAnimInstance();
		FOnMontageEnded BlendOutDelegate;
		BlendOutDelegate.BindUObject(this, &ARPG_GameCharacter::VaultMotionWrapComplete);
		animInstance->Montage_SetBlendingOutDelegate(BlendOutDelegate, vaultAnimMontage);
		animInstance->Montage_Play(vaultAnimMontage, 1.0f);
	}
}

void ARPG_GameCharacter::VaultMotionWrapComplete(UAnimMontage* animMontage, bool bInterrupted)
{
	if (!bInterrupted)
	{
		this->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
		this->SetActorEnableCollision(true);
		canWarp = false;
		vaultLandPos = FVector(0, 0, 20000.0f);
	}
}

void ARPG_GameCharacter::AddOrUpdateWarp(FName warpTargetName, FVector& location)
{
	FMotionWarpingTarget motionWarpingTarget;
	motionWarpingTarget.Name = warpTargetName;
	motionWarpingTarget.Location = location;
	motionWarpingTarget.Rotation = GetActorRotation();
	MotionWarping->AddOrUpdateWarpTarget(motionWarpingTarget);
}

void ARPG_GameCharacter::Attack(const FInputActionValue& Value)
{
	TArray<AActor*> overlappingActors;
	this->GetOverlappingActors(overlappingActors);

	for (AActor* overlappingActor : overlappingActors)
	{
		if (overlappingActor->GetClass()->ImplementsInterface(UIAssasination::StaticClass()))
		{
			AAICharacter* targetAICharacter = Cast<AAICharacter>(overlappingActor);
			if (targetAICharacter != nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("AICharacter"));

				UAnimInstance* animInstance = this->GetMesh()->GetAnimInstance();
				FOnMontageEnded BlendOutDelegate;
				targetAICharacter->StealthBackAssasin(targetAICharacterLocation, targetAICharacterRotation);

				BlendOutDelegate.BindUObject(this, &ARPG_GameCharacter::StealthAttackCallback);
				animInstance->Montage_SetBlendingOutDelegate(BlendOutDelegate, stealthAttackAnimMontage);
				animInstance->Montage_Play(stealthAttackAnimMontage, 1.0f);
			}
		}
	}
}

void ARPG_GameCharacter::StealthAttackCallback(UAnimMontage* animMontage, bool bInterrupted)
{
	if (!bInterrupted)
	{
		FMotionWarpingTarget motionWarpingTarget;
		motionWarpingTarget.Name = FName("Assasination Wrap");
		motionWarpingTarget.Location = targetAICharacterLocation;
		motionWarpingTarget.Rotation = targetAICharacterRotation;
		MotionWarping->AddOrUpdateWarpTarget(motionWarpingTarget);
	}
}

void ARPG_GameCharacter::Damage(float dmg)
{
	if (!playerStats->TakeDamage(dmg))
	{
		OnDeath();
	}
}

void ARPG_GameCharacter::Heal(float hp)
{
	playerStats->Heal(hp);
}

void ARPG_GameCharacter::GainXP(float xp)
{
	playerStats->GainXP(xp);
}

void ARPG_GameCharacter::UseMana(float mana)
{
	playerStats->UseMana(mana);
}

void ARPG_GameCharacter::GainMana(float mana)
{
	playerStats->GainMana(mana);
}

void ARPG_GameCharacter::OnDeath()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	DisableInput(PlayerController);
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}
