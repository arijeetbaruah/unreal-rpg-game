// Fill out your copyright notice in the Description page of Project Settings.

#include "AICharacter.h"
#include "Components/SphereComponent.h"
#include "Blueprint/WidgetTree.h"
#include "InteractionMessageUserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"

// Sets default values
AAICharacter::AAICharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AssasinationRadius = CreateDefaultSubobject<USphereComponent>(TEXT("AssasinationRadius"));
	AssasinationRadius->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	AssasinationRadius->OnComponentBeginOverlap.AddDynamic(this, &AAICharacter::OnAssasianationOverlapBegin);
	AssasinationRadius->OnComponentEndOverlap.AddDynamic(this, &AAICharacter::OnAssasianationOverlapEnd);
}

// Called when the game starts or when spawned
void AAICharacter::BeginPlay()
{
	Super::BeginPlay();
	isDeath = false;
}

// Called every frame
void AAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AAICharacter::StealthBackAssasin_Implementation(FVector& refPosition, FRotator& assasinRotatopm)
{
	this->GetMesh()->GetAnimInstance()->Montage_Play(stealthKillAnimMontage, 1.0f);
	Ragdoll();
}

void AAICharacter::Ragdoll()
{
	if (!isDeath)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), onDeadAudio, this->GetActorLocation());
		if (interactionWidget)
		{
			interactionWidget->RemoveFromViewport();
		}

		FTimerHandle timerHandle;
		isDeath = true;
		GetWorldTimerManager().SetTimer(timerHandle, this, &AAICharacter::RagdollDelayCallback, 2.5f, false);
	}
}

void AAICharacter::RagdollDelayCallback()
{
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

void AAICharacter::OnAssasianationOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!isDeath && OtherActor && (OtherActor != this) && OtherComp)
	{
		if (interactionWidget == nullptr)
		{
			interactionWidget = CreateWidget<UInteractionMessageUserWidget>(GetWorld(), InteractionTextWidget, FName("interaction"));
		}

		interactionWidget->AddToViewport();
	}
}

void AAICharacter::OnAssasianationOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		if (interactionWidget)
		{
			interactionWidget->RemoveFromViewport();
		}
	}
}
