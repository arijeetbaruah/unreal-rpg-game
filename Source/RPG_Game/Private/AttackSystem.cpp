// Fill out your copyright notice in the Description page of Project Settings.


#include "AttackSystem.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "EnhancedInputSubsystems.h"

// Sets default values for this component's properties
UAttackSystem::UAttackSystem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UAttackSystem::BeginPlay()
{
	Super::BeginPlay();
	
	// ...
	
}


// Called every frame
void UAttackSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UAttackSystem::SetupPlayerInputComponent(UEnhancedInputComponent* EnhancedInputComponent)
{
	EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Started, this, &UAttackSystem::AttackStart);
	EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Completed, this, &UAttackSystem::AttackEnd);
}

void UAttackSystem::AttackStart(const FInputActionValue& Value)
{
	if (canAttack)
	{
		SwordAttack();
	}
}

void UAttackSystem::AttackEnd(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Warning, TEXT("Attack End"));
}

void UAttackSystem::SwordAttack()
{
	saveAttack = isAttacking;
	if (!isAttacking)
	{
		PlaySwordAttack();
	}
}

void UAttackSystem::SwordAttackCombo()
{
	if (saveAttack)
	{
		saveAttack = false;
		PlaySwordAttack();
	}
	else
	{
		SwordAttackComboEnd();
	}
}

void UAttackSystem::SwordAttackComboEnd()
{
	isAttacking = false;
	attackIndex = 0;
}
