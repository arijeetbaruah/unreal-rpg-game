// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStats.h"
#include "LevelUpSliderWidget.h"
#include "CharacterLevelData.h"
#include "PlayerStatsHUD.h"

// Sets default values for this component's properties
UPlayerStats::UPlayerStats()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPlayerStats::BeginPlay()
{
	Super::BeginPlay();

	playerStatsHUD = CreateWidget<UPlayerStatsHUD>(GetWorld(), BPplayerStatsHUD, FName("Player Stats HUD"));
	playerStatsHUD->AddToViewport();
	playerLevelUpHUD = CreateWidget<ULevelUpSliderWidget>(GetWorld(), BPplayerLevelUpHUD, FName("Player XP HUD"));
	//playerLevelUpHUD->AddToViewport();

	xpRequirementedToLevelUp = GetRequirementedXP(level);
	previousXpRequirementedToLevelUp = level < 2 ? 0 : GetRequirementedXP(level - 1);
}


// Called every frame
void UPlayerStats::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

bool UPlayerStats::TakeDamage(float dmg)
{
	if (currentHealth != 0)
	{
		currentHealth = FMath::Max(0, currentHealth - dmg);
		playerStatsHUD->SetHP(currentHealth, maxHealth);
		return currentHealth != 0;
	}

	return false;
}

void UPlayerStats::Heal(float hp)
{
	currentHealth = FMath::Min(maxHealth, currentHealth + hp);
	playerStatsHUD->SetHP(currentHealth, maxHealth);
}

void UPlayerStats::GainXP(float xp)
{
	currentXP = currentXP + xp ;
	if (currentXP >= xpRequirementedToLevelUp)
	{
		LevelUp();
	}
	else
	{
		playerLevelUpHUD->SetXP(currentXP, xpRequirementedToLevelUp, previousXpRequirementedToLevelUp);
	}
}

void UPlayerStats::LevelUp()
{
	level++;

	int nextMaxXP = GetRequirementedXP(level);
	bool canLvlUp = xpRequirementedToLevelUp != nextMaxXP;
	previousXpRequirementedToLevelUp = xpRequirementedToLevelUp;
	xpRequirementedToLevelUp = GetRequirementedXP(level);
	playerLevelUpHUD->SetLevel(level);
	playerLevelUpHUD->SetXP(currentXP, xpRequirementedToLevelUp, previousXpRequirementedToLevelUp);
	if (currentXP >= xpRequirementedToLevelUp && canLvlUp)
	{
		LevelUp();
	}
}

int UPlayerStats::GetRequirementedXP(int lvl)
{
	lvl = FMath::Clamp(lvl, 1, xpToLevelMap->lvlData.Num());
	level = lvl;
	int xp = xpToLevelMap->lvlData[lvl - 1];
	return xp;
}

bool UPlayerStats::UseStamina()
{
	if (currentStamina > 0)
	{
		currentStamina = FMath::Max(0, currentStamina - staminaUseRate) * GetWorld()->GetDeltaSeconds();
		return true;
	}

	return false;
}

void UPlayerStats::GainStamina()
{
	currentStamina = FMath::Min(maxStamina, maxStamina + staminaRechargeRate) * GetWorld()->GetDeltaSeconds();
}

void UPlayerStats::UseMana(float mana)
{
	if (currentMana > mana)
	{
		currentMana = FMath::Min(maxMana, currentMana - mana);;
		playerStatsHUD->SetMana(currentMana, maxMana);
	}
}

void UPlayerStats::GainMana(float mana)
{
	currentMana = FMath::Min(maxMana, currentMana + mana);
	playerStatsHUD->SetMana(currentMana, maxMana);
}
