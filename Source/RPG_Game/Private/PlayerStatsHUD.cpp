// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStatsHUD.h"

void UPlayerStatsHUD::SetHP(float newHP, float maxHP)
{
	targetHP = newHP / maxHP;
}

void UPlayerStatsHUD::SetMana(float newMana, float maxMana)
{
	targetMana = newMana / maxMana;
}

//void UPlayerStatsHUD::SetXP(float newXP, float maxTargetXP, float minTargetXP)
//{
//	this->XPBar->SetVisibility(ESlateVisibility::Visible);
//	targetXP = newXP;
//	maxXP = maxTargetXP;
//	minXP = minTargetXP;
//	XPTxt->SetText(FText::FromString(FString::Printf(TEXT("%.0f/%.0f"), currentXP, maxXP)));
//}
//
//void UPlayerStatsHUD::SetLevel(int lvl)
//{
//	this->LvlTxt->SetText(FText::FromString(FString::Printf(TEXT("Level %d"), lvl)));
//}

void UPlayerStatsHUD::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
	if (currentHP != targetHP)
	{
		currentHP = FMath::FInterpTo(currentHP, targetHP, DeltaTime, HPInterpSpeed);
		this->hpBar->SetPercent(currentHP);
	}

	if (currentMana != targetMana)
	{
		currentMana = FMath::FInterpTo(currentMana, targetMana, DeltaTime, ManaInterpSpeed);
		this->Mana->SetPercent(currentMana);
	}

	/*if (currentXP != maxXP)
	{
		currentXP = FMath::FInterpTo(currentXP, maxXP, DeltaTime, XPInterpSpeed);
		currentXP = FMath::Min(currentXP, targetXP);
		this->XPBar->SetPercent((currentXP - minXP) / (maxXP - minXP));
		XPTxt->SetText(FText::FromString(FString::Printf(TEXT("%.0f/%.0f"), currentXP, maxXP)));

		if (currentXP == maxXP)
		{
			this->XPBar->SetVisibility(ESlateVisibility::Hidden);
		}
	}*/
}
