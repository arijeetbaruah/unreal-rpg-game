// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelUpSliderWidget.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"

void ULevelUpSliderWidget::SetXP(float newCurrentXP, float newMaxXP, float previousMaxXP)
{
	this->maxXP = newMaxXP;
	this->previousXP = previousMaxXP;

	AddToViewport();
	cooldown = 5;
	targetXP = newCurrentXP;
}
void ULevelUpSliderWidget::SetLevel(int newLevel)
{
	AddToViewport();
	LvlTxt->SetText(FText::FromString(FString::Printf(TEXT("Level %d"), newLevel)));
}

void ULevelUpSliderWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	if (currentXP != targetXP)
	{
		cooldown = 5;
		currentXP = FMath::FInterpTo(currentXP, targetXP, InDeltaTime, 1);
		XPTxt->SetText(FText::FromString(FString::Printf(TEXT("%0.0f/%0.0f"), currentXP, maxXP)));
		float per = (currentXP - previousXP) / (maxXP - previousXP);
		XPBar->SetPercent(per);
	}
	else {
		RemoveFromViewport();
	}
}
